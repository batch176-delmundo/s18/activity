// alert (`Batch 176`)

// Acitivity:
/*
1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
-solve the health of the pokemon that the tackle is invoked, current value of target's health should
decrease countinously as many time the tackle invoked.
(target.health - this.attack)
2. If health is below 5, invoke faint function




*/




function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;
	this.tackle = function(target) {
		console.log(this.name + ' tackled '+ target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));

		target.health -= this.attack 
		if(target.health <= 5) {
			target.faint()
		}

	},
	this.faint = function() {
		console.log(this.name +  `fainted`)
	}

}


let balbasaur = new Pokemon(`Balbasaur`, 5);
let mew = new Pokemon(`mew`, 8);
let blastoise = new Pokemon (`Blastoise`, 15)
let butterfree = new Pokemon (`Butterfree`, 15)
balbasaur.tackle(mew)
balbasaur.tackle(mew)
balbasaur.tackle(mew)
balbasaur.tackle(mew)
balbasaur.tackle(mew)

butterfree.tackle(blastoise)
butterfree.tackle(blastoise)
butterfree.tackle(blastoise)
butterfree.tackle(blastoise)

